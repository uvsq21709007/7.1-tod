package fr.uvsq.tod.springtodo; 


import org.springframework.ui.Model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class HelloController {
	
@Autowired     
	private HelloService helloService;

	    
	
@RequestMapping(method = RequestMethod.GET)	
public String afficheListeTaches(Model model) {
	System.out.println("Affichage de la liste des taches\n");
	model.addAttribute("todos",helloService.getAllTaches());
	return "greeting" ;
}



@RequestMapping(method = RequestMethod.POST)
public String nouvelleTache(String title) 
{

	helloService.AddTache(title);
    System.out.println("Tache ajoutée\n");
    return "redirect:";
}   
	    

	
	}
	
	
	
	

	
