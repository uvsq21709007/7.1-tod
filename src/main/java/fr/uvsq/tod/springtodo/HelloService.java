package fr.uvsq.tod.springtodo;


import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloService {
  
	@Autowired
	private CustomerRepository todorepository;
	private final AtomicLong compteur = new AtomicLong();
			
	
	public Iterable<Tache> getAllTaches()
	{	return todorepository.findAll();
	}
	public void AddTache(String title) {
		
		todorepository.save(new Tache(compteur.incrementAndGet(), title));
	
}
}
